#pragma once

#include <QtWidgets/QMainWindow>
#include <QCoreApplication>
#include <QMessageBox>
#include "ui_modelGenerator.h"
#include "paintwidget.h"

class modelGenerator : public QMainWindow
{
	Q_OBJECT

public:
	modelGenerator(QWidget *parent = Q_NULLPTR);

private:
	Ui::modelGeneratorClass ui;

	paintWidget w;
	QMessageBox msgBox;

private slots:
	
	void open_clicked();
	void sphere_clicked();
	void elipsoid_clicked();
};
