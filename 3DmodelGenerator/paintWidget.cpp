#include "paintwidget.h"


paintWidget::paintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

void paintWidget::Raster(int x1, int y1, int x2, int y2)
{
	double y = y1;
	double m = (double)(y2 - y1) / (x2 - x1);

	if (abs(m) <= 1)
	{
		for (int i = x1; i <= x2; i++)
		{
			if (i < image.width() && round(y) < image.height() && i >= 0 && round(y) >= 0)
				image.setPixel(i, (int)round(y), value);
			y += m;
		}
	}
	else
		Raster_steep(y1, x1, y2, x2);
}
void paintWidget::Raster_steep(int x1, int y1, int x2, int y2)
{
	if (x1 > x2)
	{
		Raster_steep(x2, y2, x1, y1);
		return;
	}

	double y = y1;
	double m = (double)(y2 - y1) / (x2 - x1);

	for (int i = x1; i <= x2; i++)
	{
		if (i < image.width() && round(y) < image.height() && i >= 0 && round(y) >= 0)
			image.setPixel((int)round(y), i, value);
		y += m;
	}
}
void paintWidget::Create_Ellipsoid_points(int n_r, int n_p, double a_, double b_, double c_)
{
	ellipsoid = true;
	poludniky = n_p;
	rovnobezky = n_r;
	QMessageBox mbox;

	double alfa = 0, dielik_alfa = 2 * M_PI / poludniky;
	double beta = 0, dielik_beta = M_PI / rovnobezky;
	QVector<double> vektor;

	if (a_ < 1)	a = 1.0;
	else		a = a_;
	if (b_ < 1)	b = 1.0;
	else		b = b_;
	if (c_ < 1)	c = 1.0;
	else		c = c_;

	vektor.push_back(a * cos(alfa) * sin(beta));
	vektor.push_back(b * sin(alfa) * sin(beta));
	vektor.push_back(c * cos(beta));
	ellipsoid_points << vektor;
	image_points << vektor;
	vektor.clear();
	beta += dielik_beta;

	for (int i = 1; i < rovnobezky; i++)
	{
		alfa = 0;
		for (int j = 0; j < poludniky; j++)
		{
			vektor.push_back(a * cos(alfa) * sin(beta));
			vektor.push_back(b * sin(alfa) * sin(beta));
			vektor.push_back(c * cos(beta));
			ellipsoid_points << vektor;
			image_points << vektor;
			vektor.clear();
			alfa += dielik_alfa;
		}
		beta += dielik_beta;
	}
	alfa = 0;
	vektor.push_back(a * cos(alfa) * sin(beta));
	vektor.push_back(b * sin(alfa) * sin(beta));
	vektor.push_back(c * cos(beta));
	ellipsoid_points << vektor;
	image_points << vektor;
	vektor.clear();
}

void paintWidget::Create_Ellipsoid_polygons()
{
	QVector<int> vektor;

	for (int i = 1; i < poludniky; i++) {
		ellipsoid_polygons.push_back(vektor << 0 << i << i + 1);
		vektor.clear();
	}
	ellipsoid_polygons.push_back(vektor << 0 << poludniky << 1);
	vektor.clear();

	for (int k = 1; k < ellipsoid_points.size() - poludniky - 1; k++)
	{
		if (k % poludniky == 0)
		{
			ellipsoid_polygons.push_back(vektor << k << k + poludniky << k + 1);
			vektor.clear();
			ellipsoid_polygons.push_back(vektor << k << k + 1 << k - poludniky + 1);
			vektor.clear();
		}
		else
		{
			ellipsoid_polygons.push_back(vektor << k << k + poludniky << k + poludniky + 1);
			vektor.clear();
			ellipsoid_polygons.push_back(vektor << k << k + poludniky + 1 << k + 1);
			vektor.clear();
		}
	}
	for (int i = ellipsoid_points.size() - 2; i > ellipsoid_points.size() - poludniky - 1; i--) {
		ellipsoid_polygons.push_back(vektor << ellipsoid_points.size() - 1 << i << i - 1);
		vektor.clear();
	}
	ellipsoid_polygons.push_back(vektor << ellipsoid_points.size() - 1 << ellipsoid_points.size() - poludniky - 1 << ellipsoid_points.size() - 2);
	vektor.clear();
}

void paintWidget::Create_Bezier_points(double hlbka, int pocet_krivka, int pocet_z)
{
	bezier = true;
	QVector<QVector<QPoint>> P;
	QVector<double> vektor;
	int y_min = INFINITY, y_max = 0, presnost;
	int n = vrcholy.size() - 1;
	P.resize(n + 1);
	for (int i = 0; i < n + 1; i++) {
		P[i].resize(n + 1);
		P[i][0] = QPoint(round((200.0 / image.width())* (vrcholy[i].x() - image.width() / 2.0)), round((200.0 / image.height())* (vrcholy[i].y() - image.height() / 2.0)));
		if (vrcholy[i].y() < y_min)
			y_min = vrcholy[i].y();
		if (vrcholy[i].y() > y_max)
			y_max = vrcholy[i].y();
	}
	if ((y_max - y_min) > image.height() / 2.0)
		presnost = round(image.width() / 10.0);
	else
		presnost = round(image.width() / 16.0);
	double dielik = 1 / (float)presnost, t = 0;

	QVector<QPoint> B;
	B.push_back(P[0][0]);
	int x, y;
	while (t < 1) {
		t += dielik;
		for (int j = 1; j <= n; j++) {
			for (int i = 0; i <= n - j; i++) {
				x = round((1.0 - t)*P[i][j - 1].x() + t * P[i + 1][j - 1].x());
				y = round((1.0 - t)*P[i][j - 1].y() + t * P[i + 1][j - 1].y());
				P[i][j] = QPoint(x, y);
			}
		}
		B.push_back(P[0][n]);
	}
	while ((B.size() - 1) % pocet_krivka != 0)
		B.removeLast();
	int z = 0;
	for (int i = 0; i < pocet_z; i++) {
		z += round(hlbka / pocet_z);
		for (int i = 0; i < B.size(); i += ((B.size() - 1) / (float)pocet_krivka)) {
			vektor.push_back(B[i].x());
			vektor.push_back(B[i].y());
			vektor.push_back(z);
			bezier_points << vektor;
			image_bezier << vektor;
			vektor.clear();
		}
	}
}

void paintWidget::Create_Bezier_polygons(int pocet_krivka, int pocet_z)
{
	QVector<int> vektor;
	int k;
	for (int i = 0; i < pocet_z - 1; i++) {
		for (int j = 0; j < pocet_krivka; j++) {
			k = j + i * (pocet_krivka + 1);
			bezier_polygons.push_back(vektor << k + pocet_krivka + 1 << k << k + 1);
			vektor.clear();
			bezier_polygons.push_back(vektor << k + pocet_krivka + 1 << k + 1 << k + pocet_krivka + 2);
			vektor.clear();
		}
	}
}

void paintWidget::Premietaj(int typ, double stred, double zen, double azim)
{
	zen = zen - zenit;
	azim = azim - azimuth;
	if (azim != -10 && zen != -10)
	{
		if (azim == 0)
			Rotate_zenit(zen);
		if (zen == 0)
			Rotate_azimuth(azim);
	}
	zenit += zen;
	azimuth += azim;
	value = qRgb(0, 0, 0);

	if (ellipsoid) {
		if (typ == 0)
			for (int k = 0; k < image_points.size(); k++)
			{
				image_points[k][0] = image.width() / 200. * ellipsoid_points[k][0] + image.width() / 2.0;
				image_points[k][1] = image.height() / 200. * ellipsoid_points[k][1] + image.height() / 2.0;
			}
		else
			for (int k = 0; k < ellipsoid_points.size(); k++)
			{
				image_points[k][0] = image.width() / 200. * ((-stred * ellipsoid_points[k][0]) / (-stred - ellipsoid_points[k][2])) + image.width() / 2.0;
				image_points[k][1] = image.height() / 200. * ((-stred * ellipsoid_points[k][1]) / (-stred - ellipsoid_points[k][2])) + image.height() / 2.0;
			}
	}

	if (bezier) {
		if (typ == 0)
			for (int k = 0; k < image_bezier.size(); k++)
			{
				image_bezier[k][0] = image.width() / 200. * bezier_points[k][0] + image.width() / 2.0;
				image_bezier[k][1] = image.height() / 200. * bezier_points[k][1] + image.height() / 2.0;
			}
		else
			for (int k = 0; k < bezier_points.size(); k++)
			{
				image_bezier[k][0] = image.width() / 200. * ((-stred * bezier_points[k][0]) / (-stred - bezier_points[k][2])) + image.width() / 2.0;
				image_bezier[k][1] = image.height() / 200. * ((-stred * bezier_points[k][1]) / (-stred - bezier_points[k][2])) + image.height() / 2.0;
			}
	}
}

void paintWidget::Rotate_zenit(double uhol)
{
	double tmp;
	if (ellipsoid) {
		for (int i = 0; i < image_points.size(); i++)
		{
			tmp = ellipsoid_points[i][0] * cos(uhol) + ellipsoid_points[i][2] * sin(uhol);
			ellipsoid_points[i][2] = ellipsoid_points[i][2] * cos(uhol) - ellipsoid_points[i][0] * sin(uhol);
			ellipsoid_points[i][0] = tmp;
		}
	}
	if (bezier) {
		for (int i = 0; i < image_bezier.size(); i++)
		{
			tmp = bezier_points[i][0] * cos(uhol) + bezier_points[i][2] * sin(uhol);
			bezier_points[i][2] = bezier_points[i][2] * cos(uhol) - bezier_points[i][0] * sin(uhol);
			bezier_points[i][0] = tmp;
		}
	}
}

void paintWidget::Rotate_azimuth(double uhol)
{
	double tmp;
	if (ellipsoid) {
		for (int i = 0; i < image_points.size(); i++)
		{
			tmp = ellipsoid_points[i][0] * cos(uhol) - ellipsoid_points[i][1] * sin(uhol);
			ellipsoid_points[i][1] = ellipsoid_points[i][1] * cos(uhol) + ellipsoid_points[i][0] * sin(uhol);
			ellipsoid_points[i][0] = tmp;
		}
	}
	if (bezier) {
		for (int i = 0; i < image_bezier.size(); i++)
		{
			tmp = bezier_points[i][0] * cos(uhol) - bezier_points[i][1] * sin(uhol);
			bezier_points[i][1] = bezier_points[i][1] * cos(uhol) + bezier_points[i][0] * sin(uhol);
			bezier_points[i][0] = tmp;
		}
	}
}


void paintWidget::Set_image(int pocet_deleni)
{
	image.fill(qRgb(255, 255, 255));

	if (ellipsoid) {
		for (int k = 0; k < ellipsoid_polygons.size(); k++)
		{
			if (image_points[ellipsoid_polygons[k][0]][0] > image_points[ellipsoid_polygons[k][1]][0])
				Raster(round(image_points[ellipsoid_polygons[k][1]][0]), round(image_points[ellipsoid_polygons[k][1]][1]), round(image_points[ellipsoid_polygons[k][0]][0]), round(image_points[ellipsoid_polygons[k][0]][1]));
			else
				Raster(round(image_points[ellipsoid_polygons[k][0]][0]), round(image_points[ellipsoid_polygons[k][0]][1]), round(image_points[ellipsoid_polygons[k][1]][0]), round(image_points[ellipsoid_polygons[k][1]][1]));
			if (image_points[ellipsoid_polygons[k][1]][0] > image_points[ellipsoid_polygons[k][2]][0])
				Raster(round(image_points[ellipsoid_polygons[k][2]][0]), round(image_points[ellipsoid_polygons[k][2]][1]), round(image_points[ellipsoid_polygons[k][1]][0]), round(image_points[ellipsoid_polygons[k][1]][1]));
			else
				Raster(round(image_points[ellipsoid_polygons[k][1]][0]), round(image_points[ellipsoid_polygons[k][1]][1]), round(image_points[ellipsoid_polygons[k][2]][0]), round(image_points[ellipsoid_polygons[k][2]][1]));
		}
	}
	if (bezier) {
		for (int k = 0; k < bezier_polygons.size(); k++)
		{
			if (image_bezier[bezier_polygons[k][0]][0] > image_bezier[bezier_polygons[k][1]][0])
				Raster(round(image_bezier[bezier_polygons[k][1]][0]), round(image_bezier[bezier_polygons[k][1]][1]), round(image_bezier[bezier_polygons[k][0]][0]), round(image_bezier[bezier_polygons[k][0]][1]));
			else
				Raster(round(image_bezier[bezier_polygons[k][0]][0]), round(image_bezier[bezier_polygons[k][0]][1]), round(image_bezier[bezier_polygons[k][1]][0]), round(image_bezier[bezier_polygons[k][1]][1]));
			if (image_bezier[bezier_polygons[k][1]][0] > image_bezier[bezier_polygons[k][2]][0])
				Raster(round(image_bezier[bezier_polygons[k][2]][0]), round(image_bezier[bezier_polygons[k][2]][1]), round(image_bezier[bezier_polygons[k][1]][0]), round(image_bezier[bezier_polygons[k][1]][1]));
			else
				Raster(round(image_bezier[bezier_polygons[k][1]][0]), round(image_bezier[bezier_polygons[k][1]][1]), round(image_bezier[bezier_polygons[k][2]][0]), round(image_bezier[bezier_polygons[k][2]][1]));
		}
		for (int k = bezier_polygons.size() - 2 * pocet_deleni + 1; k < bezier_polygons.size(); k += 2) {
			if (image_bezier[bezier_polygons[k][0]][0] > image_bezier[bezier_polygons[k][2]][0])
				Raster(round(image_bezier[bezier_polygons[k][2]][0]), round(image_bezier[bezier_polygons[k][2]][1]), round(image_bezier[bezier_polygons[k][0]][0]), round(image_bezier[bezier_polygons[k][0]][1]));
			else
				Raster(round(image_bezier[bezier_polygons[k][0]][0]), round(image_bezier[bezier_polygons[k][0]][1]), round(image_bezier[bezier_polygons[k][2]][0]), round(image_bezier[bezier_polygons[k][2]][1]));
		}
	}

}

void paintWidget::Vynuluj_uhly()
{
	zenit = 0;
	azimuth = 0;
}

int paintWidget::open_file()
{
	QMessageBox mbox;
	QString folder = settings.value("Folder path", "").toString();
	QString fileName = QFileDialog::getOpenFileName(this, "Load vtk", folder, "vtk file (*.vtk *.vtk.gz)");
	settings.setValue("Folder path", QFileInfo(fileName).absoluteDir().absolutePath());
	QFile subor(fileName);
	if (!subor.open(QIODevice::ReadOnly | QIODevice::Text))
		return 0;
	QTextStream citaj(&subor);
	QVector<double> vektor;
	ellipsoid = true;
	citaj.readLine();
	citaj.readLine();
	citaj.readLine();
	citaj.readLine();
	QString line = citaj.readLine();
	QStringList list = line.split(" ");
	int size = list[1].toInt();

	for (int i = 0; i < size; i++) {
		line = citaj.readLine();
		list = line.split(" ");
		vektor.push_back(list[0].toDouble());
		vektor.push_back(list[1].toDouble());
		vektor.push_back(list[2].toDouble());
		ellipsoid_points << vektor;
		image_points << vektor;
		vektor.clear();
	}
	line = citaj.readLine();
	list = line.split(" ");
	size = list[1].toInt();

	QVector<int> vektor_int;
	for (int i = 0; i < size; i++) {
		line = citaj.readLine();
		list = line.split(" ");
		ellipsoid_polygons.push_back(vektor_int << list[1].toInt() << list[2].toInt() << list[3].toInt());
		vektor_int.clear();
	}

	return 1;
}

void paintWidget::Write_vtk_file(int typ)
{
	QVector<int> vektor;
	QString nazov;
	QList<QVector<double>> points;
	QVector<QVector<int>> polygons;
	if (typ == 0) {
		nazov = "ellipsoid";
		points = ellipsoid_points;
		polygons = ellipsoid_polygons;
	}
	if (typ == 1) {
		nazov = "bezier";
		points = bezier_points;
		polygons = bezier_polygons;
	}
	QFile subor(nazov + ".vtk");
	if (!subor.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	QTextStream zapisuj(&subor);

	zapisuj << "# vtk DataFile Version 3.0" << endl;
	zapisuj << "vtk output" << endl << "ASCII" << endl << "DATASET POLYDATA" << endl;

	zapisuj << "POINTS " << points.size() << " float" << endl;
	for (int k = 0; k < points.size(); k++) {
		zapisuj << points[k][0] << " " << points[k][1] << " " << points[k][2] << endl;
	}

	zapisuj << "POLYGONS " << polygons.size() << " " << 4 * polygons.size() << endl;

	for (int i = 0; i < polygons.size(); i++)
	{
		zapisuj << 3 << " " << polygons[i][0] << " " << polygons[i][1] << " " << polygons[i][2] << endl;
	}

	subor.close();
}

void paintWidget::Nastav_koeficienty(double R_s, double R_d, double R_a, int ostrost, float L11, float L12, float L13, float L21, float L22, float L23, float L31, float L32, float L33, int pocet)
{
	Rs = R_s;
	Rd = R_d;
	Ra = R_a;

	h = ostrost;
	pocet_zdrojov = pocet;
	zdroje_svetla.resize(3);
	zdroje_svetla[0].push_back(L11);
	zdroje_svetla[0].push_back(L12);
	zdroje_svetla[0].push_back(L13);
	zdroje_svetla[1].push_back(L21);
	zdroje_svetla[1].push_back(L22);
	zdroje_svetla[1].push_back(L23);
	zdroje_svetla[2].push_back(L31);
	zdroje_svetla[2].push_back(L32);
	zdroje_svetla[2].push_back(L33);
	for (int i = 0; i < pocet_zdrojov; i++) {
		QMessageBox mbox;
		mbox.setText(QString::number(zdroje_svetla[i][0]) + " " + QString::number(zdroje_svetla[i][1]) + " " + QString::number(zdroje_svetla[i][2]));
		mbox.exec();
		float velkost = sqrt(zdroje_svetla[i][0] * zdroje_svetla[i][0] + zdroje_svetla[i][1] * zdroje_svetla[i][1] + zdroje_svetla[i][2] * zdroje_svetla[i][2]);
		zdroje_svetla[i][0] = zdroje_svetla[i][0] / velkost;
		zdroje_svetla[i][1] = zdroje_svetla[i][1] / velkost;
		zdroje_svetla[i][2] = zdroje_svetla[i][2] / velkost;
	}
}

void paintWidget::Nastav_farbu(QRgb zdroj, QRgb okolie)
{
	Il = zdroj;
	Io = okolie;
}

void paintWidget::Tienuj(int typ, float vzdialenost)
{
	F.resize(image.width());
	for (int i = 0; i < image.width(); i++) {
		F[i].resize(image.height());
		for (int j = 0; j < image.height(); j++)
			F[i][j] = qRgb(255, 255, 255);
	}
	Z.resize(image.width());
	for (int i = 0; i < image.width(); i++) {
		Z[i].resize(image.height());
		for (int j = 0; j < image.height(); j++)
			Z[i][j] = INFINITY;
	}

	if (typ == 0) {
		if (ellipsoid) {
			Konstantne_tienovanie(vzdialenost, ellipsoid_points, image_points, ellipsoid_polygons);
		}
		if (bezier) {
			Konstantne_tienovanie(vzdialenost, bezier_points, image_bezier, bezier_polygons);
		}
	}
	else {
		if (ellipsoid) {
			Goraudovo_tienovanie(vzdialenost, ellipsoid_points, image_points, ellipsoid_polygons);
		}
		if (bezier) {
			Goraudovo_tienovanie(vzdialenost, bezier_points, image_bezier, bezier_polygons);
		}
	}
}

void paintWidget::P_O_M(QVector<float> N, float vzdialenost)
{
	float ln, vr;
	QVector<float> suma(3, 0);
	QVector<float> R(3, 0);
	float velkost = sqrt(N[0] * N[0] + N[1] * N[1] + N[2] * N[2]);
	N[0] = N[0] / velkost;
	N[1] = N[1] / velkost;
	N[2] = N[2] / velkost;
	//	printf("%lf %lf %lf %d ", Rs, Rd, Ra, h);
		//getchar();
	for (int i = 0; i < pocet_zdrojov; i++) {

		ln = zdroje_svetla[i][0] * N[0] + zdroje_svetla[i][1] * N[1] + zdroje_svetla[i][2] * N[2];
		R[0] = 2 * ln*N[0] - zdroje_svetla[i][0];
		R[1] = 2 * ln*N[1] - zdroje_svetla[i][1];
		R[2] = 2 * ln*N[2] - zdroje_svetla[i][2];
		vr = R[0] * 0 + R[1] * 0 + R[2] * (-1.0);
		//for (int k = 1; k < h; k++)
			//vr *= vr;

		suma[0] += Il.red()*(Rs*pow(vr, h) + Rd * ln);
		suma[1] += Il.green()*(Rs*pow(vr, h) + Rd * ln);
		suma[2] += Il.blue()*(Rs*pow(vr, h) + Rd * ln);
	}
	suma[0] += Io.red()*Ra;
	suma[1] += Io.green()*Ra;
	suma[2] += Io.blue()*Ra;
	for (int i = 0; i < 3; i++) {
		if (suma[i] < 0)
			suma[i] = 0;
		if (suma[i] > 255)
			suma[i] = 255;
	}
	value = qRgb(round(suma[0]), round(suma[1]), round(suma[2]));
}

void paintWidget::Vypln_trojuholnik_konst(QVector<QVector<double>> hrany, float z)
{
	int x1, x2;
	double w;
	if (hrany[0][1] != hrany[1][1]) {
		for (int ya = int(hrany[0][1]) + 1; ya < hrany[1][1]; ya++) {
			w = (hrany[1][0] - hrany[0][0]) / (hrany[1][1] - hrany[0][1]);
			x1 = round(w*ya - w * hrany[0][1] + hrany[0][0]);
			w = (hrany[2][0] - hrany[0][0]) / (hrany[2][1] - hrany[0][1]);
			x2 = round(w*ya - w * hrany[0][1] + hrany[0][0]);
			if (x1 > x2) {
				int tmp = x1;
				x1 = x2;
				x2 = tmp;
			}
			for (int x = x1; x < x2; x++)
				if (x < image.width() && round(ya) < image.height() && x >= 0 && round(ya) >= 0)
					Z_buffer(x, ya, z);

		}
	}
	if (hrany[1][1] == hrany[2][1])
		return;
	for (int ya = round(hrany[1][1]); ya < round(hrany[2][1]); ya++) {
		w = (hrany[2][0] - hrany[0][0]) / (hrany[2][1] - hrany[0][1]);
		x1 = round(w*ya - w * hrany[0][1] + hrany[0][0]);
		w = (hrany[2][0] - hrany[1][0]) / (hrany[2][1] - hrany[1][1]);
		x2 = round(w*ya - w * hrany[1][1] + hrany[1][0]);
		if (x1 > x2) {
			int tmp = x1;
			x1 = x2;
			x2 = tmp;
		}
		for (int x = x1; x < x2; x++)
			if (x < image.width() && round(ya) < image.height() && x >= 0 && round(ya) >= 0)
				Z_buffer(x, ya, z);
	}
}

void paintWidget::Z_buffer(int x, int y, float z)
{
	if (z < Z[x][y]) {
		Z[x][y] = z;
		F[x][y] = value;
	}
}

void paintWidget::Konstantne_tienovanie(float vzdialenost, QList<QVector<double>> points, QList<QVector<double>> image_, QVector<QVector<int>> polygons)
{
	float a1, a2, a3, b1, b2, b3;
	QVector<float> normala;
	QVector<QVector<double>> hrany;
	normala.resize(3);
	hrany.resize(3);
	float y_min;
	int index_min = 0, left, right;

	for (int i = 0; i < polygons.size(); i++)
	{
		a1 = points[polygons[i][1]][0] - points[polygons[i][0]][0];
		a2 = points[polygons[i][1]][1] - points[polygons[i][0]][1];
		a3 = points[polygons[i][1]][2] - points[polygons[i][0]][2];
		b1 = points[polygons[i][2]][0] - points[polygons[i][0]][0];
		b2 = points[polygons[i][2]][1] - points[polygons[i][0]][1];
		b3 = points[polygons[i][2]][2] - points[polygons[i][0]][2];
		normala[0] = a2 * b3 - a3 * b2;
		normala[1] = a3 * b1 - a1 * b3;
		normala[2] = a1 * b2 - a2 * b1;
		P_O_M(normala, vzdialenost);
		//najdem najvyssi bod trojuholniku
		y_min = INFINITE;
		for (int k = 0; k < 3; k++) {
			if (image_[polygons[i][k]][1] < y_min) {
				y_min = image_[polygons[i][k]][1];
				index_min = k;
			}
		}
		//zoradim hrany trojuholnika
		hrany[0] = image_[polygons[i][index_min]];
		if (index_min == 0)	left = 2;
		else left = index_min - 1;
		if (index_min == 2)	right = 0;
		else right = index_min + 1;
		if (image_[polygons[i][left]][1] == image_[polygons[i][right]][1]) {	//ak su y-sur zhodne
			if (image_[polygons[i][left]][0] < image_[polygons[i][right]][0]) { //porovnam x-sur
				hrany[1] = image_[polygons[i][left]];
				hrany[2] = image_[polygons[i][right]];
			}
			else {
				hrany[2] = image_[polygons[i][left]];
				hrany[1] = image_[polygons[i][right]];
			}
		}
		else { //inak porovnam y-sur
			if (image_[polygons[i][left]][1] < image_[polygons[i][right]][1]) {
				hrany[1] = image_[polygons[i][left]];
				hrany[2] = image_[polygons[i][right]];
			}
			else {
				hrany[2] = image_[polygons[i][left]];
				hrany[1] = image_[polygons[i][right]];
			}
		}
		Vypln_trojuholnik_konst(hrany, points[polygons[i][index_min]][2]);
	}
}

void paintWidget::Goraudovo_tienovanie(float vzdialenost, QList<QVector<double>> points, QList<QVector<double>> image_, QVector<QVector<int>> polygons)
{
	QVector<float> normala;
	QVector<QVector<float>> normaly;
	QVector<QVector<double>> hrany;
	QVector<QColor> f;
	QVector<QColor> farby;
	normala.resize(3);
	farby.resize(3);
	normala[0] = 0;
	normala[1] = 0;
	normala[2] = 0;
	hrany.resize(3);
	float y_min;
	int index_min = 0, left, right;
	for (int i = 0; i < poludniky; i++) { //prvy vrchol
		normala[0] += points[polygons[i][1]][1] - points[polygons[i][0]][1] * points[polygons[i][2]][2] - points[polygons[i][0]][2] - points[polygons[i][1]][2] - points[polygons[i][0]][2] * points[polygons[i][2]][1] - points[polygons[i][0]][1];
		normala[1] += points[polygons[i][1]][2] - points[polygons[i][0]][2] * points[polygons[i][2]][0] - points[polygons[i][0]][0] - points[polygons[i][1]][0] - points[polygons[i][0]][0] * points[polygons[i][2]][2] - points[polygons[i][0]][2];
		normala[2] += points[polygons[i][1]][0] - points[polygons[i][0]][0] * points[polygons[i][2]][1] - points[polygons[i][0]][1] - points[polygons[i][1]][1] - points[polygons[i][0]][1] * points[polygons[i][2]][0] - points[polygons[i][0]][0];
	}
	normala[0] /= poludniky;
	normala[1] /= poludniky;
	normala[2] /= poludniky;
	P_O_M(normala, vzdialenost);
	f.push_back(value);
	normala[0] = 0;
	normala[1] = 0;
	normala[2] = 0;
	for (int i = 0; i < poludniky + 3; i++) { //druhy vrchol
		if (i == 2)
			i += poludniky - 2;
		normala[0] += points[polygons[i][1]][1] - points[polygons[i][0]][1] * points[polygons[i][2]][2] - points[polygons[i][0]][2] - points[polygons[i][1]][2] - points[polygons[i][0]][2] * points[polygons[i][2]][1] - points[polygons[i][0]][1];
		normala[1] += points[polygons[i][1]][2] - points[polygons[i][0]][2] * points[polygons[i][2]][0] - points[polygons[i][0]][0] - points[polygons[i][1]][0] - points[polygons[i][0]][0] * points[polygons[i][2]][2] - points[polygons[i][0]][2];
		normala[2] += points[polygons[i][1]][0] - points[polygons[i][0]][0] * points[polygons[i][2]][1] - points[polygons[i][0]][1] - points[polygons[i][1]][1] - points[polygons[i][0]][1] * points[polygons[i][2]][0] - points[polygons[i][0]][0];
	}
	normala[0] /= poludniky - 1;
	normala[1] /= poludniky - 1;
	normala[2] /= poludniky - 1;
	P_O_M(normala, vzdialenost);
	f.push_back(value);
	normala[0] = 0;
	normala[1] = 0;
	normala[2] = 0;
	for (int i = 0; i < 3 * poludniky; i++) {//treti vrchol
		if (i == 1)
			i += poludniky - 2;
		if (i == poludniky)
			i = 3 * poludniky - 2;
		normala[0] += points[polygons[i][1]][1] - points[polygons[i][0]][1] * points[polygons[i][2]][2] - points[polygons[i][0]][2] - points[polygons[i][1]][2] - points[polygons[i][0]][2] * points[polygons[i][2]][1] - points[polygons[i][0]][1];
		normala[1] += points[polygons[i][1]][2] - points[polygons[i][0]][2] * points[polygons[i][2]][0] - points[polygons[i][0]][0] - points[polygons[i][1]][0] - points[polygons[i][0]][0] * points[polygons[i][2]][2] - points[polygons[i][0]][2];
		normala[2] += points[polygons[i][1]][0] - points[polygons[i][0]][0] * points[polygons[i][2]][1] - points[polygons[i][0]][1] - points[polygons[i][1]][1] - points[polygons[i][0]][1] * points[polygons[i][2]][0] - points[polygons[i][0]][0];
	}
	normala[0] /= poludniky - 1;
	normala[1] /= poludniky - 1;
	normala[2] /= poludniky - 1;
	P_O_M(normala, vzdialenost);
	f.push_back(value);


	//najdem najvyssi bod trojuholniku
	y_min = INFINITE;
	for (int k = 0; k < 3; k++) {
		if (image_[polygons[0][k]][1] < y_min) {
			y_min = image_[polygons[0][k]][1];
			index_min = k;
		}
	}
	//zoradim hrany trojuholnika
	hrany[0] = image_[polygons[0][index_min]];
	farby[0] = f[index_min];
	if (index_min == 0)	left = 2;
	else left = index_min - 1;
	if (index_min == 2)	right = 0;
	else right = index_min + 1;
	if (image_[polygons[0][left]][1] == image_[polygons[0][right]][1]) {	//ak su y-sur zhodne
		if (image_[polygons[0][left]][0] < image_[polygons[0][right]][0]) { //porovnam x-sur
			hrany[1] = image_[polygons[0][left]];
			hrany[2] = image_[polygons[0][right]];
			farby[1] = f[left];
			farby[2] = f[right];
		}
		else {
			hrany[2] = image_[polygons[0][left]];
			hrany[1] = image_[polygons[0][right]];
			farby[2] = f[left];
			farby[1] = f[right];
		}
	}
	else { //inak porovnam y-sur
		if (image_[polygons[0][left]][1] < image_[polygons[0][right]][1]) {
			hrany[1] = image_[polygons[0][left]];
			hrany[2] = image_[polygons[0][right]];
			farby[1] = f[left];
			farby[2] = f[right];
		}
		else {
			hrany[2] = image_[polygons[0][left]];
			hrany[1] = image_[polygons[0][right]];
			farby[2] = f[left];
			farby[1] = f[right];
		}
	}

	int x1, x2, i = 1, j = 0;
	double w, z = points[polygons[0][index_min]][2];
	double d;
	QColor farba1, farba2, farbaD;
	QColor farba;
	//vyplnaj trojuholnik interpolaciou
	if (hrany[0][1] != hrany[1][1]) {
		farbaD = interpolate(farby[0], farby[2], hrany[1][1] / (hrany[2][1] - hrany[0][1]));
		d = hrany[1][1] - int(hrany[0][1]);
		for (int ya = int(hrany[0][1]) + 1; ya < hrany[1][1]; ya++) {
			w = (hrany[1][0] - hrany[0][0]) / (hrany[1][1] - hrany[0][1]);
			x1 = round(w*ya - w * hrany[0][1] + hrany[0][0]);
			w = (hrany[2][0] - hrany[0][0]) / (hrany[2][1] - hrany[0][1]);
			x2 = round(w*ya - w * hrany[0][1] + hrany[0][0]);
			if (x1 > x2) {
				int tmp = x1;
				x1 = x2;
				x2 = tmp;
			}
			farba1 = interpolate(farby[0], farby[1], i / d);
			farba2 = interpolate(farby[0], farbaD, i / d);
			i++;
			j = 0;
			for (int x = x1; x < x2; x++) {
				if (x < image.width() && round(ya) < image.height() && x >= 0 && round(ya) >= 0) {
					farba = interpolate(farba1, farba2, j / (x2 - x1)).toRgb();
					value = qRgb(farba.red(), farba.green(), farba.blue());
					Z_buffer(x, ya, z);
				}
				j++;
			}
		}
	}
	if (hrany[1][1] == hrany[2][1])
		return;
	if (hrany[0][1] == hrany[1][1])
		farbaD = farby[0];
	d = round(hrany[2][1]) - round(hrany[1][1]);
	i = 0;
	for (int ya = round(hrany[1][1]); ya < round(hrany[2][1]); ya++) {
		w = (hrany[2][0] - hrany[0][0]) / (hrany[2][1] - hrany[0][1]);
		x1 = round(w*ya - w * hrany[0][1] + hrany[0][0]);
		w = (hrany[2][0] - hrany[1][0]) / (hrany[2][1] - hrany[1][1]);
		x2 = round(w*ya - w * hrany[1][1] + hrany[1][0]);
		if (x1 > x2) {
			int tmp = x1;
			x1 = x2;
			x2 = tmp;
		}
		farba1 = interpolate(farby[1], farby[2], i / d);
		farba2 = interpolate(farbaD, farby[2], i / d);
		i++;
		j = 0;
		for (int x = x1; x < x2; x++) {
			if (x < image.width() && round(ya) < image.height() && x >= 0 && round(ya) >= 0) {
				farba = interpolate(farba1, farba2, j / (x2 - x1));
				value = qRgb(farba.red(), farba.green(), farba.blue());
				Z_buffer(x, ya, z);
			}
			j++;
		}
	}
}

void paintWidget::Set_image_color()
{
	for (int i = 0; i < image.width(); i++) {
		for (int j = 0; j < image.height(); j++)
			image.setPixel(i, j, qRgb(F[i][j].red(), F[i][j].green(), F[i][j].blue()));
	}
}

QColor paintWidget::interpolate(QColor startValue, QColor endValue, float fraction)
{
	float red = (endValue.red() - startValue.red()) * fraction + startValue.red();
	float green = (endValue.green() - startValue.green()) * fraction + startValue.green();
	float blue = (endValue.blue() - startValue.blue()) * fraction + startValue.blue();
	return qRgb(round(red), round(green), round(blue));
}

bool paintWidget::Skontroluj_vrcholy()
{
	if (vrcholy.size() == 0)
		return false;
	return true;
}

bool paintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool paintWidget::newImage(int x, int y)
{
	QImage loadedImage(x, y, QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool paintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName, "png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void paintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void paintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}


void paintWidget::clearImage()
{
	ellipsoid_points.clear();
	ellipsoid_polygons.clear();
	image_points.clear();
	bezier_points.clear();
	bezier_polygons.clear();
	image_bezier.clear();
	bezier = false;
	ellipsoid = false;
	zdroje_svetla.clear();
	F.clear();
	Z.clear();
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void paintWidget::clear_vrcholy()
{
	vrcholy.clear();
}

void paintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		vrcholy << lastPoint;
	}
}

void paintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void paintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void paintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void paintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void paintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void paintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void paintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int paintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

