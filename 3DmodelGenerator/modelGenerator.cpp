#include "modelGenerator.h"

modelGenerator::modelGenerator(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->w);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	w.newImage(500, 500);
}

void modelGenerator::open_clicked()
{
	msgBox.setText("Len tak....test :D");
	msgBox.exec();
}

void modelGenerator::sphere_clicked()
{
	w.clearImage();
	w.Vynuluj_uhly();

	w.Create_Ellipsoid_points(10, 10, 50, 50, 50);
	w.Create_Ellipsoid_polygons();
	w.Write_vtk_file(0);
	
	w.Premietaj(0, 70.0, -10., -10.);

	w.Set_image(0);

	w.update();
}

void modelGenerator::elipsoid_clicked()
{
	w.clearImage();
	w.Vynuluj_uhly();

	w.Create_Ellipsoid_points(10, 10, 100, 55, 55);
	w.Create_Ellipsoid_polygons();
	w.Write_vtk_file(0);

	w.Premietaj(0, 70.0, -10., -10.);

	w.Set_image(0);

	w.update();
}
