
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QVector>
#include <QTextStream>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>

class paintWidget : public QWidget
{
	Q_OBJECT

public:
	paintWidget(QWidget *parent = 0);

	void Raster(int x1, int y1, int x2, int y2);
	void Raster_steep(int x1, int y1, int x2, int y2);
	void Create_Ellipsoid_points(int n_r, int n_p, double a, double b, double c);
	void Create_Ellipsoid_polygons();
	void Create_Bezier_points(double hlbka, int pocet_krivka, int pocet_z);
	void Create_Bezier_polygons(int pocet_krivka, int pocet_z);
	void Premietaj(int typ, double stred, double zenit, double azimut);
	void Rotate_zenit(double uhol);
	void Rotate_azimuth(double uhol);
	void Set_image(int pocet_deleni);
	void Vynuluj_uhly();
	int open_file();
	void Write_vtk_file(int typ);
	void Nastav_koeficienty(double R_s, double R_d, double R_a, int ostrost, float L11, float L12, float L13, float L21, float L22, float L23, float L31, float L32, float L33, int pocet);
	void Nastav_farbu(QRgb zdroj, QRgb okolie);
	void Tienuj(int typ, float vzdialenost);
	void P_O_M(QVector<float> N, float vzdialenost);
	void Vypln_trojuholnik_konst(QVector<QVector<double>> hrany, float z);
	void Z_buffer(int x, int y, float z);
	void Konstantne_tienovanie(float vzdialenost, QList<QVector<double>> points, QList<QVector<double>> image_, QVector<QVector<int>> polygons);
	void Goraudovo_tienovanie(float vzdialenost, QList<QVector<double>> points, QList<QVector<double>> image_, QVector<QVector<int>> polygons);
	void Set_image_color();
	QColor interpolate(QColor startValue, QColor endValue, float fraction);
	bool Skontroluj_vrcholy();
	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }
	void clearImage();
	void clear_vrcholy();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	int selectKth(int* data, int s, int e, int k);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	QRgb value;
	QList<QVector<double>> ellipsoid_points;
	QList<QVector<double>> image_points;
	QVector<QVector<int>> ellipsoid_polygons;
	QList<QVector<double>> bezier_points;
	QList<QVector<double>> image_bezier;
	QVector<QVector<int>> bezier_polygons;
	QSettings settings;
	int poludniky;
	int rovnobezky;
	double a, b, c;
	double zenit = 0, azimuth = 0;
	double Rs, Rd, Ra;
	int h;
	int pocet_zdrojov;
	bool bezier = false;
	bool ellipsoid = false;
	QVector<QVector<float>> zdroje_svetla;
	QColor Il, Io;
	QVector<QVector<QColor>> F;
	QVector<QVector<float>> Z;
	QList<QPoint> vrcholy;
};